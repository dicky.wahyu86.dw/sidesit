<div class="page-header">
	<h1 class="page-title">Pengelolaan Data <?= $title; ?></h1>
	<div class="page-header-actions">
		<div class="btn-group btn-group-sm" id="withBtnGroup" aria-label="Page Header Actions" role="group">
			<button type="button" class="btn btn-primary">
				<i class="icon <?= $icform; ?>" aria-hidden="true"></i>
				<span class="hidden-sm-down">Kode Form: <?= $idf; ?></span>
			</button>
			<button type="button" class="btn btn-danger" data-toggle="tooltip" data-original-title="Refresh Data" data-container="body" onclick="refreshdata()">
				<i class="icon wb-loop" aria-hidden="true"></i>
			</button>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xxl-9 col-lg-12 col-md-12">
		<div class="panel">
			<div class="panel-heading"><h3 class="panel-title">Data</h3></div>
			<div class="panel-body">
                <ul class="nav nav-pills " role="tablist">
                    <?php 
                        $ab = 0;
                        foreach($dataAkun as $row):
                        $ab++;
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($ab == '1'){echo'active';} ?>" href="#data-<?= $ab;?>" role="tab" data-toggle="tab">
                            <?= $row->nama; ?> 
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
                <div class="tab-content tab-space">
                    <?php 
                        $a=0;
                        $debit = 0;
                        $kredit = 0;
                        
                        for($i=0;$i<$jumlah;$i++) :                          
                            $a++;
                            $s=0;
                            $deb = $saldo[$i];
                    ?>
                    <div class="tab-pane <?php if($a == '1'){echo'active';} ?>" id="data-<?=$a?>">
                        <table class="table table-hover table-striped w-full">
                            <thead>
                                <tr>
                                    <th rowspan="2">Tanggal</th>
                                    <th rowspan="2">Keterangan </th>
                                    <th rowspan="2">Debit</th>
                                    <th rowspan="2">Kredit</th>
                                    <th colspan="2" class="text-center">Saldo</th>
                                </tr>
                                <tr>
                                    <th>Debit</th>
                                    <th>Kredit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for($j=0;$j<count($data_akun[$i]);$j++):
                                ?>
                                <tr>
                                    <td><?= date('m-d-Y', strtotime($data_akun[$i][$j]->tgl_tran)); ?></td>
                                    <td><?= $data_akun[$i][$j]->nama ?></td>
                                    <?php if($data_akun[$i][$j]->status =='debit'){ ?>
                                        <td>
                                            <?= 'Rp. '.number_format($data_akun[$i][$j]->nominal,0,',','.') ?>
                                        </td>
                                        <td>Rp. 0</td>
                                    <?php }else{ ?>
                                        <td>Rp. 0</td>
                                        <td>
                                            <?= 'Rp. '.number_format($data_akun[$i][$j]->nominal,0,',','.') ?>
                                        </td>
                                    <?php } ?>
                                    <?php
                                    if($deb[$j]->status =="debit"){
                                        $debit = $debit + $deb[$j]->nominal;
                                    }else{
                                        $kredit = $kredit + $deb[$j]->nominal;
                                    }

                                    $hasil = $debit-$kredit;
                                    ?>
                                    <?php if($hasil>=0){ ?>
                                    <td><?= 'Rp. '.number_format($hasil,0,',','.') ?></td>
                                    <td> - </td>
                                    <?php }else{ ?>
                                    <td> - </td>
                                    <td><?= 'Rp. '.number_format(abs($hasil),0,',','.') ?></td>
                                    <?php } ?>
                                </tr>
                                <?php endfor ?>
                                <tr class="bg-dark">
                                    <?php
                                    $debit = 0;
                                    $kredit = 0;
                                    ?>
                                    <td class="text-center" colspan="4"><b>Total</b></td>
                                    <?php if($hasil>=0){ ?>
                                    <td class="text-success">
                                        <b><?= 'Rp. '.number_format($hasil,0,',','.') ?></b>
                                    </td>
                                    <td> - </td>
                                    <?php }else{ ?>
                                    <td> - </td>
                                    <td class="text-danger"><b><?= 'Rp. '.number_format(abs($hasil),0,',','.') ?></b></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endfor; ?>
                </div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {
		$('#tbl-xdt').DataTable();
		var a = $('#debit').html();
		var b = $('#kredit').html();
		if(a == b){
			$(".bg").css("background-color", "#11c26d");
			$('#hasil').html("<b>SEIMBANG</b>");
		}else{
			$(".bg").css("background-color", "#dc3545");
			$('#hasil').html("<b>TIDAK SEIMBANG</b>");
		}
	});
	var idmenu = "<?= $idm; ?>";
	var idform = "<?= ucfirst($idf); ?>";
	$("#tpm" + idmenu).addClass("active");
	$("#stpm" + idform).addClass("active");

	// swal("Sedang Mengakses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
	// var tabel = $('#tbl-xdt').DataTable({
	// 	"ajax": "<?= base_url(ucfirst($idf).'/json'); ?>",
	// 	"fnDrawCallback": function(oSettings){swal.close();}
	// });

	refresh();

	function refreshdata(){
		swal("Sedang Mengakses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		tabel.ajax.reload(null, false);
	}
	
	function refresh(){
		$("#status").hide();
		$("#id_akun").show();
		$("#nominal").show();
		$("#ket").show();
        $("#txtid").val("Otomatis By System");
        $("#cboidakun").val("").change();
		$("#cbojt").val("+").change();
		$("#txtnominal").val("");
		$("#txtketerangan").val("");
        $("#lbljudul").html('Form Tambah Data');
        $("#bloktombol").html('<button type="button" class="btn btn-success" id="btntambah" onclick="tambah()">Tambahkan</button>&nbsp<button type="button" class="btn btn-primary" id="btnrefresh" onclick="refresh()">Batal</button>');
	}
	
	function tambah(){
		$("#btntambah").attr("disabled", true);
        var id = $("#txtid").val();
        var id_akun = $("#cboidakun").val();
		var nominal = $("#txtnominal").val();
    	var keterangan = $("#txtketerangan").val();

        if(id == "" || id_akun == "" || nominal == "" || keterangan == ""){
            swal({title: 'Tambah Gagal', text: 'Ada Isian yang Belum Anda Isi !', icon: 'error'});
            return;
        }
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
        $.ajax({
            url: "<?= base_url(ucfirst($idf).'/tambah'); ?>",
            method: "POST",
            data: {id: id, id_akun: id_akun, nominal: nominal, keterangan: keterangan},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
                if(y == 1){
                    swal({
						title: 'Tambah Berhasil',
						text: 'Data Akun Berhasil di Tambahkan',
						icon: 'success'
					}).then((Refreshh)=>{
						location.reload();
						// refresh();
						// tabel.ajax.reload(null, false);
					});
                }else{
					if(y == 99){
						swal({title: 'Tambah Gagal', text: 'Anda Tidak Memiliki Akses Menambah Data Pada Menu Ini', icon: 'error'});
						refresh();
					}else{
						swal({title: 'Tambah Gagal', text: 'Ada Beberapa Masalah dengan Data yang Anda Isikan !', icon: 'error'});
					}
                }
            },
			error: function(){
				swal.close();
				swal({title: 'Tambah Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})
		$("#btntambah").attr("disabled", false);
	}

	function update(){
		$("#btnupdate").attr("disabled", true);
		$("#btnhapus").attr("disabled", true);
		$("#btnrefresh").attr("disabled", true);

		var id = $("#txtid").val();
		var status_data = $("#status_data").val();
        // var id_akun = $("#cboidakun").val();
		// var jenis_transaksi = $("#cbojt").val();
		// var nominal = $("#txtnominal").val();
		// var keterangan = $("#txtketerangan").val();
    
        if(id == "" || status_data == ""){
            swal({title: 'Update Gagal', text: 'Ada Isian yang Belum Anda Isi !', icon: 'error'});
            return;
        }
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
        $.ajax({
            url: "<?= base_url(ucfirst($idf).'/update'); ?>",
            method: "POST",
            data: {id: id, status:status_data},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
                if(y == 1){
                    swal({
						title: 'Update Berhasil',
						text: 'Data Akun Berhasil di Update',
						icon: 'success'
					}).then((Refreshh)=>{
						location.reload();
						// refresh();
						// tabel.ajax.reload(null, false);
					});
                }else{
					if(y == 99){
						swal({title: 'Update Gagal', text: 'Anda Tidak Memiliki Akses Update Data Pada Menu Ini', icon: 'error'});
						refresh();
					}else{
						swal({title: 'Update Gagal', text: 'Ada Beberapa Masalah dengan Data yang Anda Isikan !', icon: 'error'});
					}
                }
            },
			error: function(){
				swal.close();
				swal({title: 'Update Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})

		$("#btnupdate").attr("disabled", false);
		$("#btnhapus").attr("disabled", false);
		$("#btnrefresh").attr("disabled", false);
	}
/*
	function hapus(){
		$("#btnupdate").attr("disabled", true);
		$("#btnhapus").attr("disabled", true);
		$("#btnrefresh").attr("disabled", true);

        var id = $("#txtid").val();
    
        if(id == ""){
            swal({title: 'Hapus Gagal', text: 'ID Akun Kosong !', icon: 'error'});
            return;
		}
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		swal({
			title: 'Hapus Data',
			text: "Anda Yakin Ingin Menghapus Data Ini ?",
			icon: 'warning',
			buttons:{
				confirm: {text : 'Yakin', className : 'btn btn-success'},
				cancel: {visible: true, text: 'Tidak', className: 'btn btn-danger'}
			}
		}).then((Hapuss)=>{
			if(Hapuss){
				$.ajax({
					url: "<?= base_url(ucfirst($idf).'/hapus'); ?>",
					method: "POST",
					data: {id: id},
					cache: "false",
					success: function(x){
						swal.close();
						var y = atob(x);
						if(y == 1){
							swal({
								title: 'Hapus Berhasil',
								text: 'Data Akun Berhasil di Hapus',
								icon: 'success'
							}).then((Refreshh)=>{
								refresh();
								tabel.ajax.reload(null, false);
							});
						}else{
							if(y == 99){
								swal({title: 'Hapus Gagal', text: 'Anda Tidak Memiliki Akses Menghapus Data Pada Menu Ini', icon: 'error'});
								refresh();
							}else{
								if(y == 90){
									swal({title: 'Hapus Gagal', text: 'Data Menu Ini Masih digunakan dalam Data Log History, Sehingga Tidak Dapat di Hapus Hanya Dapat di Ubah', icon: 'error'});
									refresh();
								}else{
									swal({title: 'Hapus Gagal', text: 'Periksa Kembali Data Yang Anda Pilih !', icon: 'error'});
								}
							}
						}
					},
					error: function(){
						swal.close();
						swal({title: 'Hapus Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
					}
				})
			}else{swal.close();}
		});

		$("#btnupdate").attr("disabled", false);
		$("#btnhapus").attr("disabled", false);
		$("#btnrefresh").attr("disabled", false);
	}
*/	
	function filter(el){
		var id = $(el).data("id");
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		$.ajax({
            url: "<?= base_url($idf.'/filter'); ?>",
            method: "POST",
            data: {id: id},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
				var xx = y.split("|");
				if(xx[0] == 1){
					$("#status").show();
					$("#id_akun").hide();
					$("#nominal").hide();
					$("#ket").hide();
					$("#txtid").val(xx[1]);
					// $("#cboidakun").val(":hidden");
					// $("#txtnominal").val(xx[3]);
					// $("#txtketerangan").val(xx[4]);
					$("#lbljudul").html('Form Kelola Data');
					// $("#txtusername").attr("readonly", true);
					$("#bloktombol").html('\
						<button type="button" class="btn btn-info" id="btnupdate" onclick="update()">Update</button>\
						<button type="button" class="btn btn-primary" id="btnrefresh" onclick="refresh()">Batal</button>\
					');
				}else{
					swal({title: 'Update Gagal', text: 'Data Tidak di Temukan', icon: 'error'});
					refresh();
				}
            },
			error: function(){
				swal.close();
				swal({title: 'Filter Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})
    }

	// function reset(el){
	// 	var id = $(el).data("id");
	// 	swal({
	// 		title: 'Reset Password',
	// 		text: "Anda Yakin Ingin Reset Password Akun Ini ?",
	// 		icon: 'warning',
	// 		buttons:{
	// 			confirm: {text : 'Yakin', className : 'btn btn-success'},
	// 			cancel: {visible: true, text: 'Tidak', className: 'btn btn-danger'}
	// 		}
	// 	}).then((Resett)=>{
	// 		if(Resett){
	// 			$.ajax({
	// 				url: "<?= base_url(ucfirst($idf).'/reset'); ?>",
	// 				method: "POST",
	// 				data: {id: id},
	// 				cache: "false",
	// 				success: function(x){
	// 					var y = atob(x);
	// 					if(y == 1){
	// 						swal({
	// 							title: 'Reset Berhasil', 
	// 							text: 'Password Berhasil di Reset', 
	// 							icon: 'success'
	// 						}).then((Refreshh)=>{
	// 							refresh();
	// 							tabel.ajax.reload(null, false);
	// 						});
	// 					}else{
	// 						if(y == 90){
	// 							swal({
	// 								title: 'Reset Gagal', 
	// 								text: 'Akun Yang Anda Reset, Tidak di Temukan', 
	// 								icon: 'error'
	// 							}).then((Refreshh)=>{
	// 								refresh();
	// 								tabel.ajax.reload(null, false);
	// 							});
	// 						}else{
	// 							swal({title: 'Reset Gagal', text: 'Silahkan Coba Beberapa Saat Lagi !', icon: 'error'});
	// 						}
	// 					}
	// 				},
	// 				error: function(){
	// 					swal.close();
	// 					swal({title: 'Reset Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
	// 				}
	// 			})
	// 		}else{swal.close();}
	// 	});
 //    }
</script>
			