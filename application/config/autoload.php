<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database','session','encryption');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','form','AN_fungsi','file');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('Mlogin','Mlog','Mdata','Msi933','Mme776','Mle409','Mfo110','Mak755','Mdr699','Mbp008','Mda008','Mtr008','Mbs008');
