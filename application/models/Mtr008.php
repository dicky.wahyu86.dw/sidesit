<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mtr008 extends CI_Model {
	public function data(){
		$sql = "SELECT 008_transaksi1.id AS id_tran, 008_akuntansi1.nama, 008_transaksi1.nominal, 008_transaksi1.status_data, 008_transaksi1.keterangan AS ket_tran, 008_transaksi1.tgl_buat AS tgl_tran, 008_akuntansi1.status FROM 008_transaksi1 INNER JOIN 008_akuntansi1 ON 008_transaksi1.id_akun=008_akuntansi1.id ORDER BY id_tran";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	public function filter($a){
		$sql = "SELECT * FROM 008_transaksi1 WHERE id='$a' ORDER BY id";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	// public function filterusername($a){
	// 	$sql = "SELECT * FROM akses WHERE username='$a' ORDER BY id";
	// 	$querySQL = $this->db->query($sql);
	// 	if($querySQL){return $querySQL->result();}
	// 	else{return 0;}
	// }

	public function cekform($a){
		$sql = "SELECT * FROM log_history WHERE id_user='$a' ORDER BY id";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	public function tambah($id_akun, $nominal, $keterangan){
		$user = $this->Mlogin->ambiluser();
		$sql = "INSERT INTO 008_transaksi1 VALUES(UNIX_TIMESTAMP(NOW()),'$id_akun','$nominal','$keterangan',NOW(),'0000-00-00','$user','','aktif');";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

	public function update($id, $status_data){
		$user = $this->Mlogin->ambiluser();
		$sql = "UPDATE 008_transaksi1 SET status_data='$status_data', tgl_update=NOW(), id_update='$user' WHERE id='$id';";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

	public function hapus($id){
		$sql = "DELETE FROM 008_transaksi1 WHERE id='$id';";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

	// public function reset($id, $pass){
	// 	$user = $this->Mlogin->ambiluser();
	// 	$sql = "UPDATE akses SET password='$pass', tgl_update=NOW(), id_update='$user' WHERE id='$id';";
	// 	$querySQL = $this->db->query($sql);
	// 	if($querySQL){return "1";}
	// 	else{return "0";}	
	// }
}