<div class="page-header">
	<h1 class="page-title">Pengelolaan Data Jurnal Umum</h1>
	<div class="page-header-actions">
		<div class="btn-group btn-group-sm" id="withBtnGroup" aria-label="Page Header Actions" role="group">
			<button type="button" class="btn btn-primary">
				<i class="icon <?= $icform; ?>" aria-hidden="true"></i>
				<span class="hidden-sm-down">Kode Form: <?= $idf; ?></span>
			</button>
			<button type="button" class="btn btn-danger" data-toggle="tooltip" data-original-title="Refresh Data" data-container="body" onclick="refreshdata()">
				<i class="icon wb-loop" aria-hidden="true"></i>
			</button>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xxl-9 col-lg-9 col-md-9">
		<div class="panel">
			<div class="panel-heading"><h3 class="panel-title">Data</h3></div>
			<div class="panel-body">
				<table class="table table-hover table-striped w-full" id="tbl-xdt">
					<thead>
						<tr>
							<th>No</th>
							<th style="width: 5%;">Reff</th>
							<th style="width: 15%;">Tanggal</th>
							<th style="width: 25%;">Nama Akun</th>
							<th style="width: 15%;">Debit</th>
							<th style="width: 15%;">Kredit</th>
							<th style="width: 20%;">Keterangan</th>
							<th style="width: 10%;">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach($transaksi as $data) : ?>
							<?php if($data->status_data == 'nonaktif'){ ?>
								<tr>
									<th style="color:#ccd5db"><?= $no++; ?></th>
									<th style="color:#ccd5db"><?=  $data->id_tran; ?></th>
									<th style="color:#ccd5db"><?= date('m-d-Y', strtotime($data->tgl_tran)); ?></th>
									<th style="color:#ccd5db">
										<?php 
											if($data->status == 'debit') {
												echo'<p class="text-left">'.$data->nama.'</p>';
											}else{
												echo'<p class="text-right">'.$data->nama.'</p>';
											}
										?>
									</th>
									<th style="color:#ccd5db">Rp.
										<?php if($data->status == 'debit') {
												echo number_format($data->nominal);
											}else{
												echo'0';
											} ?>
									</th>
									<th style="color:#ccd5db">Rp. 
										<?php if($data->status == 'kredit') {
												echo number_format($data->nominal);
											}else{
												echo'0';
											} ?>
									</th>
									<th style="color:#ccd5db"><?=  $data->ket_tran; ?></th>
									<th><button class='btn btn-icon btn-round btn-info' data-id='<?= $data->id_tran; ?>' onclick='filter(this)'><i class='icon wb-pencil'></i></button></th>
								</tr>	
							<?php }else{ ?>
								<tr>
									<th><?= $no++; ?></th>
									<th><?=  $data->id_tran; ?></th>
									<th><?= date('m-d-Y', strtotime($data->tgl_tran)); ?></th>
									<th>
										<?php 
											if($data->status == 'debit') {
												echo'<p class="text-left">'.$data->nama.'</p>';
											}else{
												echo'<p class="text-right">'.$data->nama.'</p>';
											}
										?>
									</th>
									<th>Rp.
										<?php if($data->status == 'debit') {
												echo number_format($data->nominal);
											}else{
												echo'0';
											} ?>
									</th>
									<th>Rp. 
										<?php if($data->status == 'kredit') {
												echo number_format($data->nominal);
											}else{
												echo'0';
											} ?>
									</th>
									<th><?=  $data->ket_tran; ?></th>
									<th><button class='btn btn-icon btn-round btn-info' data-id='<?= $data->id_tran; ?>' onclick='filter(this)'><i class='icon wb-pencil'></i></button></th>
								</tr>
							<?php } ?>		
						<?php endforeach ?>
					</tbody>
				</table>

				<table class="table table-hover table-striped w-full">
					<thead>
						<tr>
							<th style="width: 5%;"></th>
							<th style="width: 5%;"></th>
							<th style="width: 15%;"></th>
							<th style="width: 25%;"></th>
							<th style="width: 15%;">Debit</th>
							<th style="width: 15%;">Kredit</th>
							<th style="width: 20%;"></th>
							<th style="width: 10%;"></th>
						</tr>
					</thead>
					<tr>
						<th></th>
						<th colspan="3"><b> Total</b></th>
						<?php foreach($total_debet as $debit) : ?>
							<th id="debit" class="font-weight-bold">Rp. <?= number_format($debit); ?></th>
						<?php endforeach ?>
						<?php foreach($total_kredit as $kredit) : ?>
							<th id="kredit" class="font-weight-bold">Rp. <?= number_format($kredit); ?></th>
						<?php endforeach ?>
						<th></th>
						<th></th>
					</tr>
					<tr class="text-center bg">
						<td colspan="8" class="text-white" style="font-weight:bolder;font-size:19px">
							<span id="hasil"></span>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xxl-3 col-lg-3 col-md-3">
		<div class="panel">
			<div class="panel-heading"><h3 class="panel-title" id="lbljudul">Form</h3></div>
			<div class="panel-body">
				<div class="form-row">
					<div class="form-group col-md-12">
						<label class="form-control-label">ID Transaksi*</label>
						<input type="text" class="form-control khusus_angka jedaobyek" id="txtid" value="Otomatis By System" readonly autocomplete="off">
					</div>
					<div class="form-group col-md-12 jedaobyek" style="display: none;" id="status">
						<label class="form-control-label" style="margin-bottom: -5px;">Status Data</label>
						<select class="form-control" id="status_data">
							<option value="aktif">Aktif</option>
							<option value="nonaktif">Non Aktif</option>
						</select>
					</div>
					<div class="form-group col-md-12 jedaobyek" style="display: block;" id="id_akun">
						<label class="form-control-label" style="margin-bottom: -5px;">ID Akun*</label>
						<select class="form-control select2" id="cboidakun" >
							<option value="">Silahkan Pilih</option>
							<?php
								if(is_array($dtakun)){
									if(count($dtakun)>0){
										foreach($dtakun as $x){
											echo "<option value='".$x->id."'>".$x->nama." - ".$x->status."</option>";
										}
									}
								}
							?>
						</select>
					</div>
					<!-- <div class="form-group col-md-12 jedaobyek">
						<label class="form-control-label" style="margin-bottom: -5px;">Jenis Transaksi*</label>
						<select class="form-control select2" id="cbojt">
							<option value="+">+</option>
							<option value="-">-</option>
						</select>
					</div> -->
					<div class="form-group col-md-12 jedaobyek" style="display: block;" id="nominal">
						<label class="form-control-label">Nominal*</label>
						<input type="text" class="form-control khusus_abjad jedaobyek" id="txtnominal" placeholder="Masukkan Nominal Dengan Benar !" autocomplete="off" maxlength="100">
					</div>
					<div class="form-group col-md-12 jedaobyek" style="display: block;" id="ket">
						<label class="form-control-label">Keterangan*</label>
						<input type="text" class="form-control khusus_abjad jedaobyek" id="txtketerangan" placeholder="Masukkan Keterangan Transaksi !" autocomplete="off" maxlength="50">
					</div>
					<div class="form-group col-md-12" id="bloktombol"></div>
                </div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {
		$('#tbl-xdt').DataTable();
		var a = $('#debit').html();
		var b = $('#kredit').html();
		if(a == b){
			$(".bg").css("background-color", "#11c26d");
			$('#hasil').html("<b>SEIMBANG</b>");
		}else{
			$(".bg").css("background-color", "#dc3545");
			$('#hasil').html("<b>TIDAK SEIMBANG</b>");
		}
	});
	var idmenu = "<?= $idm; ?>";
	var idform = "<?= ucfirst($idf); ?>";
	$("#tpm" + idmenu).addClass("active");
	$("#stpm" + idform).addClass("active");

	// swal("Sedang Mengakses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
	// var tabel = $('#tbl-xdt').DataTable({
	// 	"ajax": "<?= base_url(ucfirst($idf).'/json'); ?>",
	// 	"fnDrawCallback": function(oSettings){swal.close();}
	// });

	refresh();

	function refreshdata(){
		swal("Sedang Mengakses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		tabel.ajax.reload(null, false);
	}
	
	function refresh(){
		$("#status").hide();
		$("#id_akun").show();
		$("#nominal").show();
		$("#ket").show();
        $("#txtid").val("Otomatis By System");
        $("#cboidakun").val("").change();
		$("#cbojt").val("+").change();
		$("#txtnominal").val("");
		$("#txtketerangan").val("");
        $("#lbljudul").html('Form Tambah Data');
        $("#bloktombol").html('<button type="button" class="btn btn-success" id="btntambah" onclick="tambah()">Tambahkan</button>&nbsp<button type="button" class="btn btn-primary" id="btnrefresh" onclick="refresh()">Batal</button>');
	}
	
	function tambah(){
		$("#btntambah").attr("disabled", true);
        var id = $("#txtid").val();
        var id_akun = $("#cboidakun").val();
		var nominal = $("#txtnominal").val();
    	var keterangan = $("#txtketerangan").val();

        if(id == "" || id_akun == "" || nominal == "" || keterangan == ""){
            swal({title: 'Tambah Gagal', text: 'Ada Isian yang Belum Anda Isi !', icon: 'error'});
            return;
        }
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
        $.ajax({
            url: "<?= base_url(ucfirst($idf).'/tambah'); ?>",
            method: "POST",
            data: {id: id, id_akun: id_akun, nominal: nominal, keterangan: keterangan},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
                if(y == 1){
                    swal({
						title: 'Tambah Berhasil',
						text: 'Data Akun Berhasil di Tambahkan',
						icon: 'success'
					}).then((Refreshh)=>{
						location.reload();
						// refresh();
						// tabel.ajax.reload(null, false);
					});
                }else{
					if(y == 99){
						swal({title: 'Tambah Gagal', text: 'Anda Tidak Memiliki Akses Menambah Data Pada Menu Ini', icon: 'error'});
						refresh();
					}else{
						swal({title: 'Tambah Gagal', text: 'Ada Beberapa Masalah dengan Data yang Anda Isikan !', icon: 'error'});
					}
                }
            },
			error: function(){
				swal.close();
				swal({title: 'Tambah Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})
		$("#btntambah").attr("disabled", false);
	}

	function update(){
		$("#btnupdate").attr("disabled", true);
		$("#btnhapus").attr("disabled", true);
		$("#btnrefresh").attr("disabled", true);

		var id = $("#txtid").val();
		var status_data = $("#status_data").val();
        // var id_akun = $("#cboidakun").val();
		// var jenis_transaksi = $("#cbojt").val();
		// var nominal = $("#txtnominal").val();
		// var keterangan = $("#txtketerangan").val();
    
        if(id == "" || status_data == ""){
            swal({title: 'Update Gagal', text: 'Ada Isian yang Belum Anda Isi !', icon: 'error'});
            return;
        }
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
        $.ajax({
            url: "<?= base_url(ucfirst($idf).'/update'); ?>",
            method: "POST",
            data: {id: id, status:status_data},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
                if(y == 1){
                    swal({
						title: 'Update Berhasil',
						text: 'Data Akun Berhasil di Update',
						icon: 'success'
					}).then((Refreshh)=>{
						location.reload();
						// refresh();
						// tabel.ajax.reload(null, false);
					});
                }else{
					if(y == 99){
						swal({title: 'Update Gagal', text: 'Anda Tidak Memiliki Akses Update Data Pada Menu Ini', icon: 'error'});
						refresh();
					}else{
						swal({title: 'Update Gagal', text: 'Ada Beberapa Masalah dengan Data yang Anda Isikan !', icon: 'error'});
					}
                }
            },
			error: function(){
				swal.close();
				swal({title: 'Update Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})

		$("#btnupdate").attr("disabled", false);
		$("#btnhapus").attr("disabled", false);
		$("#btnrefresh").attr("disabled", false);
	}
/*
	function hapus(){
		$("#btnupdate").attr("disabled", true);
		$("#btnhapus").attr("disabled", true);
		$("#btnrefresh").attr("disabled", true);

        var id = $("#txtid").val();
    
        if(id == ""){
            swal({title: 'Hapus Gagal', text: 'ID Akun Kosong !', icon: 'error'});
            return;
		}
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		swal({
			title: 'Hapus Data',
			text: "Anda Yakin Ingin Menghapus Data Ini ?",
			icon: 'warning',
			buttons:{
				confirm: {text : 'Yakin', className : 'btn btn-success'},
				cancel: {visible: true, text: 'Tidak', className: 'btn btn-danger'}
			}
		}).then((Hapuss)=>{
			if(Hapuss){
				$.ajax({
					url: "<?= base_url(ucfirst($idf).'/hapus'); ?>",
					method: "POST",
					data: {id: id},
					cache: "false",
					success: function(x){
						swal.close();
						var y = atob(x);
						if(y == 1){
							swal({
								title: 'Hapus Berhasil',
								text: 'Data Akun Berhasil di Hapus',
								icon: 'success'
							}).then((Refreshh)=>{
								refresh();
								tabel.ajax.reload(null, false);
							});
						}else{
							if(y == 99){
								swal({title: 'Hapus Gagal', text: 'Anda Tidak Memiliki Akses Menghapus Data Pada Menu Ini', icon: 'error'});
								refresh();
							}else{
								if(y == 90){
									swal({title: 'Hapus Gagal', text: 'Data Menu Ini Masih digunakan dalam Data Log History, Sehingga Tidak Dapat di Hapus Hanya Dapat di Ubah', icon: 'error'});
									refresh();
								}else{
									swal({title: 'Hapus Gagal', text: 'Periksa Kembali Data Yang Anda Pilih !', icon: 'error'});
								}
							}
						}
					},
					error: function(){
						swal.close();
						swal({title: 'Hapus Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
					}
				})
			}else{swal.close();}
		});

		$("#btnupdate").attr("disabled", false);
		$("#btnhapus").attr("disabled", false);
		$("#btnrefresh").attr("disabled", false);
	}
*/	
	function filter(el){
		var id = $(el).data("id");
		swal("Memproses Data.....", {button: false, closeOnClickOutside: false, closeOnEsc: false});
		$.ajax({
            url: "<?= base_url($idf.'/filter'); ?>",
            method: "POST",
            data: {id: id},
            cache: "false",
            success: function(x){
				swal.close();
				var y = atob(x);
				var xx = y.split("|");
				if(xx[0] == 1){
					$("#status").show();
					$("#id_akun").hide();
					$("#nominal").hide();
					$("#ket").hide();
					$("#txtid").val(xx[1]);
					// $("#cboidakun").val(":hidden");
					// $("#txtnominal").val(xx[3]);
					// $("#txtketerangan").val(xx[4]);
					$("#lbljudul").html('Form Kelola Data');
					// $("#txtusername").attr("readonly", true);
					$("#bloktombol").html('\
						<button type="button" class="btn btn-info" id="btnupdate" onclick="update()">Update</button>\
						<button type="button" class="btn btn-primary" id="btnrefresh" onclick="refresh()">Batal</button>\
					');
				}else{
					swal({title: 'Update Gagal', text: 'Data Tidak di Temukan', icon: 'error'});
					refresh();
				}
            },
			error: function(){
				swal.close();
				swal({title: 'Filter Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
			}
		})
    }

	// function reset(el){
	// 	var id = $(el).data("id");
	// 	swal({
	// 		title: 'Reset Password',
	// 		text: "Anda Yakin Ingin Reset Password Akun Ini ?",
	// 		icon: 'warning',
	// 		buttons:{
	// 			confirm: {text : 'Yakin', className : 'btn btn-success'},
	// 			cancel: {visible: true, text: 'Tidak', className: 'btn btn-danger'}
	// 		}
	// 	}).then((Resett)=>{
	// 		if(Resett){
	// 			$.ajax({
	// 				url: "<?= base_url(ucfirst($idf).'/reset'); ?>",
	// 				method: "POST",
	// 				data: {id: id},
	// 				cache: "false",
	// 				success: function(x){
	// 					var y = atob(x);
	// 					if(y == 1){
	// 						swal({
	// 							title: 'Reset Berhasil', 
	// 							text: 'Password Berhasil di Reset', 
	// 							icon: 'success'
	// 						}).then((Refreshh)=>{
	// 							refresh();
	// 							tabel.ajax.reload(null, false);
	// 						});
	// 					}else{
	// 						if(y == 90){
	// 							swal({
	// 								title: 'Reset Gagal', 
	// 								text: 'Akun Yang Anda Reset, Tidak di Temukan', 
	// 								icon: 'error'
	// 							}).then((Refreshh)=>{
	// 								refresh();
	// 								tabel.ajax.reload(null, false);
	// 							});
	// 						}else{
	// 							swal({title: 'Reset Gagal', text: 'Silahkan Coba Beberapa Saat Lagi !', icon: 'error'});
	// 						}
	// 					}
	// 				},
	// 				error: function(){
	// 					swal.close();
	// 					swal({title: 'Reset Gagal', text: 'Jaringan Anda Bermasalah !', icon: 'error'});
	// 				}
	// 			})
	// 		}else{swal.close();}
	// 	});
 //    }
</script>
			