<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mda008 extends CI_Model {
	public function data(){
		$sql = "SELECT * FROM 008_akuntansi1 ORDER BY id DESC";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	public function filter($a){
		$sql = "SELECT * FROM 008_akuntansi1 WHERE id='$a' ORDER BY id";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	public function cekform($a){
		$sql = "SELECT * FROM form_level WHERE id_level='$a' ORDER BY id";
		$querySQL = $this->db->query($sql);
		if($querySQL){return $querySQL->result();}
		else{return 0;}
	}

	public function tambah($id, $nama, $keterangan, $status){
		$user = $this->Mlogin->ambiluser();
		$sql = "INSERT INTO 008_akuntansi1 VALUES('$id','$nama','$keterangan','$user','',NOW(),'0000-00-00','$status');";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

	public function update($id, $nama, $keterangan, $status){
		$user = $this->Mlogin->ambiluser();
		$sql = "UPDATE 008_akuntansi1 SET nama='$nama', keterangan='$keterangan', tgl_update=NOW(), id_update='$user', status='$status' WHERE id='$id';";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

	public function hapus($id){
		$sql = "DELETE FROM 008_akuntansi1 WHERE id='$id';";
		$querySQL = $this->db->query($sql);
		if($querySQL){return "1";}
		else{return "0";}	
	}

}