<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tr008 extends CI_Controller {
	public $idsc;
	public $aksesc = array();
	function __construct() {
		parent::__construct();
		$idformini = "tr008";
		$data["datalogin"] = $this->Mlogin->cek_login();
		$dataform = $this->Mlogin->cek_sistem($idformini);
        if(is_array($data["datalogin"])){
			foreach($dataform as $cx){
				$idsistem_sistem = $cx->id_sistem;
			}
			$idsistem_user = array();
         	foreach ($data["datalogin"] as $dl){
				$idlevel = $dl->id_level;
				array_push($idsistem_user, $dl->id_sistem);
			}
			if(array_search($idsistem_sistem, $idsistem_user) !== false){}else{redirect(base_url());}
			$data["datamenu"] = $this->Mlogin->cek_menu($idsistem_sistem, $idlevel);
			$data["dataform"] = $this->Mlogin->cek_form($idsistem_sistem, $idlevel);
			$data["ids"] = $idsistem_sistem;
			$data["idf"] = $idformini;
			$this->idsc = $idsistem_sistem;
			$idform = array(); $akses = array();
			foreach ($data["dataform"] as $dx){
				array_push($idform, $dx->id_form);
				if($dx->id_form == $idformini){array_push($akses, $dx->akses_tambah, $dx->akses_update, $dx->akses_hapus, $dx->akses_cetak);}
			}
			if(array_search($idformini, $idform) !== false){
				$data["akses"] = $akses;
				$this->aksesc = $akses; 
			}else{redirect(base_url());}
        	$this->load->view($idsistem_sistem.'/basis', $data, true);
        }else{redirect(base_url());}
    }

	public function index(){
		$data["fill"] = "tr008v";
		$data["dtakun"] = $this->Mda008->data();
		$data["transaksi"] = $this->Mtr008->data();
		$data['total_debet'] = $this->db->query("SELECT SUM(008_transaksi1.nominal) AS debet FROM 008_transaksi1 INNER JOIN 008_akuntansi1 ON 008_transaksi1.id_akun=008_akuntansi1.id WHERE 008_akuntansi1.status='debit' AND 008_transaksi1.status_data='aktif'")->row();
		
		$data['total_kredit'] = $this->db->query("SELECT SUM(008_transaksi1.nominal) AS kredit FROM 008_transaksi1 INNER JOIN 008_akuntansi1 ON 008_transaksi1.id_akun=008_akuntansi1.id WHERE 008_akuntansi1.status='kredit' AND 008_transaksi1.status_data='aktif'")->row();
		$this->load->view($this->idsc.'/basis', $data);
	}

	public function json(){
        $dtJSON = '{"data": [xxx]}';
        $dtisi = "";
        $dt = $this->Mtr008->data();
        foreach ($dt as $k){
            $id = $k->id;
            $id_akun = $k->id_akun;
			$nominal = $k->nominal;
			$keterangan = $k->keterangan;
			$tomboledit = "<button class='btn btn-icon btn-round btn-danger' data-id='".$id."' onclick='hapus()'><i class='icon wb-trash'></i></button>";
            $dtisi .= '["'.$tomboledit.'","'.$id.'","'.$id_akun.'","'.$nominal.'","'.$keterangan.'"],';
        }
        $dtisifix = rtrim($dtisi, ",");
        $data = str_replace("xxx", $dtisifix, $dtJSON);
        echo $data;
	}

	public function filter(){
		$id = trim($this->input->post("id"));
		$dt = $this->Mtr008->filter($id);
		if(is_array($dt)){
			if(count($dt) > 0){
				foreach ($dt as $k){
					$id = $k->id;
		            $id_akun = $k->id_akun;
					$nominal = $k->nominal;
					$keterangan = $k->keterangan;
				}
				echo base64_encode("1|".$id."|".$id_akun."|".$nominal."|".$keterangan);
			}else{echo base64_encode("0|");}
		}else{echo base64_encode("0|");}
	}
	
	public function tambah(){
		if($this->aksesc[0] == "1"){
			$id_akun = trim(str_replace("'","''",$this->input->post("id_akun")));
			$nominal = trim(str_replace("'","''",$this->input->post("nominal")));
			$keterangan = trim(str_replace("'","''",$this->input->post("keterangan")));
			$operasi = $this->Mtr008->tambah($id_akun, $nominal, $keterangan);
			if($operasi == "1"){
				$ket = "Nama Data Akun: $id_akun, \nNominal: $nominal,\nKeterangan: $keterangan";
				$this->Mlog->log_history("008_transaksi","Tambah",$ket);
			}
			echo base64_encode($operasi);
		}else{echo base64_encode("99");}
	}

	public function update(){
		if($this->aksesc[1] == "1"){
			$id = trim(str_replace("'","''",$this->input->post("id")));
			$status_data = trim(str_replace("'","''",$this->input->post("status")));

			// $id_akun = trim(str_replace("'","''",$this->input->post("id_akun")));
			// $jenis_transaksi = trim(str_replace("'","''",$this->input->post("jenis_transaksi")));
			// $nominal = trim(str_replace("'","''",$this->input->post("nominal")));
			// $keterangan = trim(str_replace("'","''",$this->input->post("keterangan")));
			
			$operasi = $this->Mtr008->update($id, $status_data);
			if($operasi == "1"){
				$ket = "ID: $id,\nUpdate Status Data: $status_data";
				$this->Mlog->log_history("008_transaksi","Update",$ket);
			}
			echo base64_encode($operasi);
		}else{echo base64_encode("99");}
	}

	public function hapus(){
		if($this->aksesc[2] == "1"){
			$id = trim(str_replace("'","''",$this->input->post("id")));
			$user = $this->Mlogin->ambiluser();
			$td = $this->Mtr008->cekform($user);
			if(is_array($td)){
				if(count($td) > 0){echo base64_encode("90");}
				else{
					$dt = $this->Mtr008->filter($id);
					$operasi = $this->Mtr008->hapus($id);
					if($operasi == "1"){
						foreach ($dt as $k){
							$id = $k->id;
							$id_akun = $k->id_akun;
							$jenis_transaksi = $k->jenis_transaksi;
							$nominal = $k->nominal;
							$keterangan =$k->keterangan;
						}
						$ket = "ID Transaksi: $id,\nID Akun: $id_akun,\nJenis Transaksi: $jenis_transaksi,\nNominal: $nominal,\nKeterangan: $keterangan";
						$this->Mlog->log_history("008_transaksi","Hapus",$ket);
					}
					echo base64_encode($operasi);
				}
			}else{echo base64_encode("80");}
		}else{echo base64_encode("99");}
	}

	// public function reset(){
	// 	$id = trim(str_replace("'","''",$this->input->post("id")));
	// 	$td = $this->Mak755->filter($id);
	// 	if(is_array($td)){
	// 		if(count($td) > 0){
	// 			foreach ($td as $k){
	// 				$id = $k->id;
	// 				$nama = $k->nama;
	// 				$username = $k->username;
	// 			}
	// 			$pass =  md5(base64_encode(enkripsi($username)));
	// 			$operasi = $this->Mak755->reset($id, $pass);
	// 			if($operasi == "1"){
	// 				$ket = "ID Akun: $id,\nNama Akun: $nama,\nUsername: $username";
	// 				$this->Mlog->log_history("Akses","Reset Password",$ket);
	// 			}
	// 			echo base64_encode($operasi);
	// 		}else{echo base64_encode("90");}
	// 	}else{echo base64_encode("80");}
	// }
}
